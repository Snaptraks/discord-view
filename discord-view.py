import json
import os
from glob import glob
import dateutil.parser
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pandas.plotting import register_matplotlib_converters
import cartopy
import cartopy.crs as ccrs
from progressbar import progressbar

import geopy
from cache_to_disk import cache_to_disk
GN = geopy.geocoders.Nominatim(user_agent='discord-view')

register_matplotlib_converters()
rc('figure', figsize=(12, 9), dpi=200)


DATADIR = 'DiscordData'
# Check if the Discord Data folder exists
try:
    os.makedirs(DATADIR, exist_ok=False)
    print('DiscordData folder created. Please paste your data in it.')
    input('Press ENTER key...')
    exit()
except FileExistsError:
    # Folder exists. Continuing.
    pass


class AttrDict(dict):
    """ Dictionary subclass whose entries can be accessed by attributes
        (as well as normally).
        https://stackoverflow.com/questions/38034377/object-like-attribute-access-for-nested-dictionary
    """

    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

    @staticmethod
    def from_nested_dict(data):
        """ Construct nested AttrDicts from nested dictionaries. """
        if not isinstance(data, dict):
            return data
        else:
            return AttrDict({key: AttrDict.from_nested_dict(data[key])
                             for key in data})


class Index:
    """ Index to get the channel and server information from the IDs.
        Also contains the IDs of every channel and server visited by the user.
    """
    with open(f'{DATADIR}/messages/index.json') as f:
        channels = json.load(f)

    with open(f'{DATADIR}/servers/index.json') as f:
        servers = json.load(f)


class Account:
    """ Information about the user's account.
        Your friends list, settings, username, email, avatar, connected accounts
        and any transaction history for Nitro. If you have created any developer
        applications, this will be included as well.
    """
    avatar = f'{DATADIR}/account/avatar.png'

    with open(f'{DATADIR}/account/user.json', 'r') as f:
        user = AttrDict.from_nested_dict(json.load(f))

    if os.path.exists(f'{DATADIR}/account/applications'):
        applications = {}
        for dir in next(os.walk(f'{DATADIR}/account/applications'))[1]:
            app_dir = f'{DATADIR}/account/applications/{dir}'

            with open(f'{app_dir}/application.json', 'r') as f:
                app = json.load(f)

            # these files might not be present
            bot_avatar = f'{app_dir}/bot-avatar.png'
            if os.path.isfile(bot_avatar):
                app['bot_avatar'] = bot_avatar
            else:
                app['bot_avatar'] = None

            icon = f'{app_dir}/icon.png'
            if os.path.isfile(icon):
                app['icon'] = icon
            else:
                app['icon'] = None

            applications[dir] = AttrDict(app)


class Server:
    """ Information about a server the user was in.
        If you own a server, it will also include that server’s icon, settings,
        roles, permissions, channels, audit log, and any custom emoji attached to
        it.
    """

    def __init__(self, id):
        server_dir = f'{DATADIR}/servers/{id}/'

        for file in next(os.walk(server_dir))[2]:
            if not file.endswith('.json'):
                continue  # skip non JSON files

            attr = file[:file.index('.')].replace('-', '_')

            with open(server_dir + file, 'r') as f:
                data = AttrDict.from_nested_dict(json.load(f))

            setattr(self, attr, data)

        try:
            if self.guild.owner_id == Account.user.id:
                self.guild.is_owner = True
            else:
                self.guild.is_owner = False
        except AttributeError:
            # not server owner_id
            self.guild.is_owner = False


class Channel:
    """Information about a channel the user sent messages in."""

    def __init__(self, id):
        channel_dir = f'{DATADIR}/messages/{id}'

        with open(f'{channel_dir}/channel.json') as f:
            self.channel = AttrDict.from_nested_dict(json.load(f))

        self.channel.name = Index.channels[id]

        messages = pd.read_csv(f'{channel_dir}/messages.csv')
        messages['Timestamp'] = messages['Timestamp'].apply(
            dateutil.parser.parse)
        messages.sort_values(by='Timestamp', inplace=True)
        try:
            messages['Guild'] = self.channel.guild.id
        except AttributeError:
            pass
        messages['Channel'] = self.channel.id
        messages.rename(columns={'Contents': 'Messages'}, inplace=True)
        self.messages = messages


def activity():
    """ Detailed information of a user's activity.
        Information about how you navigate the app, the times you used Discord,
        what games you played, and what servers you hung out in. We use this to
        improve Discord and tailor it to you.

        Analytics - This is a record of some of your actions within Discord.
            The information is used to improve your Discord experience.
            You can disable tracking analytics here: User Settings > Privacy &
            Safety > Turn Off 'Use data to improve Discord'
        Modeling - This is data we use to better tailor certain parts of Discord
            to you, like the Games Tab.
            You can disable this here: User Settings > Privacy & Safety > Turn
            Off 'Use data to customize my Discord experience'
        Reporting - This folder contains a JSON file with data we use in order
            to operate our business. (Information such as messages sent, or your
            Nitro subscription, as an example.)
        Trust & Safety - This folder contains data we use in order to protect
            our service from bad actors. This data is deleted after 180 days.

    """
    names = ['analytics', 'modeling', 'reporting', 'tns']
    mask = ['city', 'region_code', 'country_code']

    def join_columns(x):
        try:
            return ', '.join(x)
        except TypeError:
            return np.nan
            
    print('Generating Activity Figures...')
    for name in progressbar(names):
        data = load_activity(name)
        locations = data[mask].apply(join_columns, axis=1).value_counts()
        a = np.array([[locations.max(), 1], [locations.min(), 1]])
        b = np.array([2000, 50])  # ADJUST IF NECESSARY
        x = np.linalg.solve(a, b)
        size = np.poly1d(x)(locations.values)

        # Location map
        positions = []
        for loc in locations.index:
            positions.append(get_lon_lat(loc))
        positions = np.asarray(positions)

        ax = plt.axes(projection=ccrs.Mercator())
        # ax.stock_img()
        # ax.coastlines()
        ax.add_feature(cartopy.feature.GSHHSFeature(scale='intermediate',
                                                    levels=[1, 2],
                                                    edgecolor='k'))
        ax.scatter(*positions.T,
                   s=size,
                   c='r',
                   edgecolors='k',
                   alpha=0.5,
                   transform=ccrs.Geodetic())
        plt.suptitle(f'{name.title()} Activity Map')
        plt.savefig(f'Figures/activity/{name}_map.png')
        plt.close()

        fig, ax = plt.subplots()
        locations.plot.bar(ax=ax, width=0.9)
        ax.set_ylabel('Frequency')
        ax.set_yscale('log')
        ax.set_title(f'{name.title()} Histogram')
        fig.tight_layout()
        fig.savefig(f'Figures/activity/{name}_hist.png')
        plt.close(fig)


def load_activity(name):
    # print(f'Loading {name} activity data...')
    raw_data = []
    for file in glob(f'{DATADIR}/activity/{name}/*.json'):
        # print(file)
        with open(file, 'r', encoding='utf-8') as f:
            raw_data += json.load(f)
    
    return pd.DataFrame(raw_data)


def messages():
    """ Creates figures about themessages sent.
        All messages you have sent in servers, DMs or group DMs, unless it has
        been deleted manually by you or someone else. This folder will also
        include links to attachments uploaded.
    """
    # 2017-07-19 16:21:57.908000+00:00
    # '%Y-%m-%d %H:%M:S.%f%z'
    # dt = dateutil.parser.parse('2017-07-19 16:21:57.908000+00:00')
    # DiscordData/messages/337267833635340299/messages.csv

    big_df = pd.DataFrame()

    # Individual channels
    # for ch_id in Index.channels.keys():
    print('Generating Channel Figures...')
    for ch_id in progressbar(Index.channels.keys()):
        ch = Channel(ch_id)
        df = ch.messages

        big_df = pd.concat([big_df, df], axis=0, ignore_index=True, sort=False)

        fig, ax = plt.subplots(nrows=2, sharex=True)

        try:
            fig.suptitle(f'{ch.channel.guild.name} #{ch.channel.name}')
        except AttributeError:
            if ch.channel.type != 3:
                fig.suptitle(ch.channel.name)
            else:
                fig.suptitle('Group Direct Messages')

        if df.empty:
            # do not create histograms if the channel has no messages
            pass

        else:
            df.set_index('Timestamp', drop=False, inplace=True)
            df = df[['Messages', 'Attachments']]
            df = df.groupby(pd.Grouper(freq='M')).count()
            df.index = df.index.strftime('%B %Y')
            df.plot.bar(ax=ax, width=1, subplots=True)
            xticks = ax[1].get_xticks()
            xticklabels = ax[1].get_xticklabels()
            nxticks = len(xticks) // 10 + 1  # might need fix
            ax[1].set_xticks(xticks[::nxticks])
            ax[1].set_xticklabels(xticklabels[::nxticks])
            fig.text(0.04, 0.5, 'Number of Messages',
                     va='center', rotation='vertical')
            fig.autofmt_xdate()  # Orient xlabels correctly
            fig.savefig(f'Figures/messages/{ch_id}_messages.png')
        plt.close(fig)

    # All channels
    big_df.sort_values(by='Timestamp', axis='index', inplace=True)

    # Number of messages sent VS Time
    print('Generating Timeline Figure...')
    big_df['Number'] = np.arange(1, len(big_df) + 1)
    fig, ax = plt.subplots()
    ax.plot(big_df['Timestamp'], big_df['Number'])
    ax.set_xlabel('Time')
    ax.set_ylabel('Total number of messages sent')
    fig.autofmt_xdate()  # Orient xlabels correctly
    fig.savefig('Figures/messages/timeline.png')
    plt.close(fig)

    def func(pct, allvals):
        absolute = pct / 100. * np.sum(allvals)
        return f'{pct:.1f}%\n({absolute:.0f})' if pct > 2 else ''

    # Favorite servers
    print('Generating Favorite Servers Figure...')
    fig, ax = plt.subplots()
    temp = big_df[big_df['Guild'].notnull()]['Guild'].value_counts()

    # Puts the guild's name if the amount of messages is above a threshold (2%)
    guilds_name = []
    for id, v in zip(temp.index, temp):
        if v > 0.02 * temp.sum():
            if Index.servers[id] is not None:
                guilds_name.append(Index.servers[id])
            else:
                guilds_name.append('Unknown Server')
        else:
            guilds_name.append('')
    ax.pie(temp,
           labels=guilds_name,
           autopct=lambda p: func(p, temp))
    fig.suptitle('Favorite Servers')
    fig.savefig('Figures/messages/favorite_servers.png')
    plt.close(fig)

    # Favorite DMs
    print('Generating Favorite DMs Figure...')
    fig, ax = plt.subplots()
    temp = big_df[~big_df['Guild'].notnull()]['Channel'].value_counts()

    # Puts the channel's name if the amount of messages is above a threshold
    # (2%)
    channels_name = []
    for id, v in zip(temp.index, temp):
        if v > 0.02 * temp.sum():
            if Index.channels[id] is not None:
                channels_name.append(Index.channels[id].split()[-1])
            else:
                channels_name.append('Unknown Direct Messages')
        else:
            channels_name.append('')

    ax.pie(temp,
           labels=channels_name,
           autopct=lambda p: func(p, temp))
    fig.suptitle('Favorite DMs')
    fig.savefig('Figures/messages/favorite_dms.png')
    plt.close(fig)


def programs():
    """ Any applications you have submitted for HypeSquad, Server Verification,
        the Partners Program, and other cool stuff.
    """
    pass


def servers():
    """ Currently does nothing. """
    for server_id in Index.servers.keys():
        s = Server(server_id)


def main():

    # Create folders for Figures
    os.makedirs('Figures/', exist_ok=True)
    os.makedirs('Figures/messages', exist_ok=True)
    os.makedirs('Figures/activity', exist_ok=True)

    # Create figures for messages
    messages()

    # Create digures for activity
    activity()

    print('Data analysis was successful.')
    input('Press ENTER key...')


@cache_to_disk()
def get_lon_lat(location):
    geoloc = GN.geocode(location)
    return [geoloc.longitude, geoloc.latitude]


if __name__ == '__main__':
    main()
