import json
import os
from glob import glob


def only_json(files):
    return [f for f in files if f.endswith('.json')]


def reformat_json():
    for root, dirs, files in os.walk('DiscordData/'):
        for fjson in only_json(files):
            path = f'{root}/{fjson}'

            multiline_flag = False

            with open(path, 'r', encoding='utf-8') as f:
                try:
                    data = json.load(f)
                except json.decoder.JSONDecodeError as e:
                    f.seek(0)
                    multiline_flag = True
                    data = []
                    for line in f.readlines():
                        data.append(json.loads(line))

            with open(path, 'w', encoding='utf-8') as f:
                json.dump(data, f, indent=4, sort_keys=True,
                          ensure_ascii=False)


if __name__ == '__main__':
    reformat_json()
