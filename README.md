# Discord Data Viewer

This script uses your Discord personnal data and renders it into meaningful
graphs for easy visualization. Written in Python 3.6.8.

## Installation

Either do:
- Download the project and execute `python discord-view.py` (see **Requirements**).
- Download the executable from the [Releases](https://gitlab.com/Snaptraks/discord-view/releases)
page.

## Usage

The script *needs* that you have your previously requested [Discord Data](https://support.discordapp.com/hc/en-us/articles/360004027692)
for it to work. Just extract it in the `DiscordData` folder at the same level as
the script/executable (it will be created if it doesn't exist at first execution).

If using the non-compiled version, run with

```python discord-view.py```

If using the compiled version, just run the file `discord-view.exe`.

## Output

Different figures will be generated:

- `Figures/messages/<ID>_messages.png`: Figures representing the amount of messages over time for a given channel/direct messages.
- `Figures/messages/favorite_servers.png`: Figure representing a pie chart of the servers you posted the most.
- `Figures/messages/favorite_dms.png`: Figure representing a pie chart of the direct messages you posted the most.
- `Figures/messages/timeline.png`: Figure representing the total number of messages sent as a function of time.

## Requirements

As stated above, you need to have your personnal data requested from Discord
for this to work.

For the non-compiled version, the script has been tested with:

- Python 3.7.3
- matplotlib 3.0.3
- numpy 1.16.2
- pandas 0.24.2

## Bonus

Included is a script named `reformat.py` that is used to rewrite the way the
data files are written. It mainly indents the JSON objects properly as to make
it human readable instead of having all the information in one line. It should
not modify any data itself therefore making it safe to use before
`discord-view`. The CSV files being much more readable by default are not
modified.
