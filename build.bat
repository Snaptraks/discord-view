@ECHO OFF

REM RMDIR /S /Q dist\discord-view

IF EXIST "discord-view.spec" (
    pyinstaller -F discord-view.spec
) ELSE (
    pyinstaller -F --exclude-module PyQt5 discord-view.py
)

COPY .\dist\discord-view.exe .\